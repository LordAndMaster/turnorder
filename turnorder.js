$(function() {
	init();
});

function init() {
	var unitList = new UnitList();
	unitList.create();

	$('#pieceorder table').append(unitList.$el);
	$('form.addUnit').submit(function() {
		try {
			unitList.addUnitFormHandler(this);
		} catch (err) {
			console.error(err);
		}
		return false;
	});

	var lastClickRecent = false;
	$('.next-turn').click(function() {
		if (lastClickRecent) {
			alert("Beware the power of double-click! DENIED.");
			return;
		}
		setTimeout(function() { lastClickRecent = false; }, 200);
		unitList.scanToNextUnitAction();
		lastClickRecent = true;
	});

	$('.randomize').click(function() {
		unitList.seedActionBars();
	});

	// Import CSV of units
	$('input.doc').change(function() {
		var doc = this.files[0];
		var reader = new FileReader();
		reader.onload = function() {
			var contents = reader.result;
			var lines = contents.split("\n");
			_.each(lines, function (line) {
				var values = line.split(",");
				if (!values || values.length < 2 || isNaN(parseInt(values[1]))) {
					return;
				}
				var fields = {
					name: values[0],
					baseSpeed: values[1],
				};
				unitList.addUnit(fields);
			});
		};
		reader.readAsText(doc);
	});

	var statuses = [
		'burn',
		'bleed',
		'root',
		'stun',
		'daze',
		'silence',
		'fear',
		'taunt',
		'strength',
		'haste',
		'sprint',
		'fortified',
		'invis',
		'weak',
		'slow',
		'snare',
		'exposed',
	];

	var $select = $('select.status');
	_.each(statuses, function (status) {
		var option = $('<option value="' + status + '">' + status + '</option>');
		$select.append(option);
	});

	$('select.status').select2();
	$('select.unit').select2();

	$('form.addStatus').submit(function() {
		try {
			unitList.addStatusFormHandler(this);
		} catch (err) {
			console.error(err);
		}
		return false;
	});

	$('form.addCustom').submit(function() {
		try {
			unitList.addStatusFormHandler(this);
		} catch (err) {
			console.error(err);
		}
		return false;
	});
}

var Game = {
	Settings: {
		actionBarFull: 1000,
		hasteMod: 1.5,
		slowMod: 0.66666666,
	},
};

var Unit = function (values) {
	values = values || {};
	this.name = values.name;
	this.actionBarFill = parseInt(values.actionBarFill) || 0;
	this.baseSpeed = parseInt(values.baseSpeed) || 1;
	this.hasteMod = 1;
	this.slowMod = 1;
	this.statuses = {};
	this.onSpeedChanged = function() {};

	this.numTurnsHad = 0;
	this.manualActionBarOverride = 0;
};

Unit.prototype.addEffect = function (effect, duration, extend) {
	switch (effect) {
		case "haste":
			this.hasteMod = Game.Settings.hasteMod;
			this.statuses.slow = 0;
			this.onSpeedChanged(this);
			break;
		case "slow":
			this.slowMod = Game.Settings.slowMod;
			this.statuses.haste = 0;
			this.onSpeedChanged(this);
			break;
	}

	if (extend) {
		this.statuses[effect] += duration;
	} else {
		this.statuses[effect] = duration;
	}
};

Unit.prototype.removeEffect = function (effect) {
	switch (effect) {
		case "haste":
			this.hasteMod = 1;
			this.onSpeedChanged(this);
			break;
		case "slow":
			this.slowMod = 1;
			this.onSpeedChanged(this);
			break;
	}

	this.statuses[effect] = 0;
};

Unit.prototype.create = function() {
	var self = this;
	this.$el = $('<tr><td><span class="name"></span><div class="statuses"></div></td><td class="speed"></td></tr>');
	this.$el.append('<td class="num-turns"></td>');
	this.$el.append('<td class="time-left"></td>');
	this.$el.append('<td class="action-bar"><input type="text" size="4"/><button>Apply</button></td>')

	this.$el.find('.action-bar button').click(function() {
		self.actionBarFill = parseInt($(this).prev().val()) || 0;
		if (self.$el.hasClass('active-turn')) {
			self.manualActionBarOverride = self.actionBarFill;
		} else {
			self.onSpeedChanged(self);
		}
	});
	this.$el.find('.action-bar input').keypress(function (e) {
		if (e.which === 13) {
			self.$el.find('.action-bar button').click();
			return false;
		}
	});

	this.$statusEl = this.$el.find('.statuses');

	this.render();
};

Unit.prototype.render = function() {
	var self = this;
	this.$statusEl.html('');

	_.each(this.statuses, function (dur, eff) {
		if (dur < 1) {
			return;
		}
		var $el = $('<div>' + eff + ': ' + dur + '</div>');
		var remEff = $('<a href="#">x</a>');
		remEff.click(function(eff, $el) {
			return function() {
				self.removeEffect(eff);
				$el.remove();
			};
		}(eff, $el));
		$el.append(remEff);
		self.$statusEl.append($el);
	});

	this.$el.find('.name').html(this.name);
	this.$el.find('.speed').html(this.baseSpeed);
	this.$el.find('.num-turns').html(this.numTurnsHad);
	this.$el.find('.time-left').html(this.getTicksToFillActionBar().toFixed(2));
	this.$el.find('.action-bar input').val(this.actionBarFill);
};

Unit.prototype.kill = function() {
	this.$el.remove();
};

Unit.prototype.tickActionBar = function (count) {
	if (count <= 0) {
		return;
	}
	this.actionBarFill += Math.round(count * this.baseSpeed * this.slowMod * this.hasteMod);
};

Unit.prototype.getTicksToFillActionBar = function() {
	var timeLeft = Game.Settings.actionBarFull - this.actionBarFill;
	var ticksLeft = timeLeft / (this.baseSpeed * this.slowMod * this.hasteMod);
	return ticksLeft;
};

Unit.prototype.resetActionBar = function() {
	this.actionBarFill = this.manualActionBarOverride;
	this.manualActionBarOverride = 0;
	this.onSpeedChanged(this);
};

Unit.prototype.endTurn = function() {
	this.$el.removeClass('active-turn');

	_.each(this.statuses, function (dur, eff) {
		if (dur > 0) {
			this.statuses[eff] --;
			if (this.statuses[eff] < 1) {
				this.removeEffect(eff);
			}
		}
	}, this);

	this.resetActionBar();
};

Unit.prototype.beginTurn = function() {
	this.numTurnsHad ++;
	this.$el.addClass('active-turn');
};

var UnitList = function() {
	this.units = [];
	this._firstScan = true;
};

UnitList.prototype.create = function() {
	this.$el = $('<tbody class="pieces"></tbody>');
};

UnitList.prototype.addUnit = function (fields) {
	if (!fields || !fields.name || !fields.baseSpeed) {
		console.warn("Invalid input to addUnit", fields);
		return;
	}

	var duplicate = _.findWhere(this.units, { name: fields.name });
	if (duplicate) {
		alert("Another unit has the same name");
		return;
	}

	var option = $('<option></option');
	option.val(fields.name);
	option.html(fields.name);
	$('select.unit').append(option);
	$('select.unit').trigger('change.select2');

	var self = this;
	var newUnit = new Unit(fields);
	newUnit.create();
	newUnit.onSpeedChanged = function(unit) {
		if (!self.$el.hasClass('active-turn')) {
			self.updateSpotInList(unit);
		}
	};

	this.updateSpotInList(newUnit);
};

UnitList.prototype.addUnitFormHandler = function (form) {
	var fields = {};
	$(form).find('input').each(function() {
		if (!this.name) {
			return;
		}
		fields[this.name] = this.value;
		this.value = null;
	});
	$(form).find('input:first').focus();

	this.addUnit(fields);
};

UnitList.prototype.addStatusFormHandler = function (form) {
	var unit = _.findWhere(this.units, { name: form.unit.value });
	var status = form.custom_status.value || form.status.value;
	unit.addEffect(status, form.turns.value);
	unit.render();

	form.turns.value = null;
	form.custom_status.value = null;
};

UnitList.prototype.updateSpotInList = function (newUnit) {
	var ndx = this.units.indexOf(newUnit);
	if (ndx >= 0) {
		this.units.splice(ndx, 1);
	}

	var newUnitTimeLeft = newUnit.getTicksToFillActionBar();

	var added = !(_.every(this.units, function (unit, ndx) {
		if (unit.getTicksToFillActionBar() > newUnitTimeLeft) {
			this.units.splice(ndx, 0, newUnit);
			newUnit.$el.insertBefore(unit.$el);
			return false;
		}
		return true;
	}, this));

	if (!added) {
		this.units.push(newUnit);
		this.$el.append(newUnit.$el);
	}

	newUnit.render();
};

// Assumes the list is already sorted!
UnitList.prototype.scanToNextUnitAction = function() {
	nextUnitNdx = 0;

	if (this._firstScan) {
		this._firstScan = false;
	} else {
		this.units[0].endTurn();
	}

	var nextUnit = this.units[nextUnitNdx];
	var ticksLeft = nextUnit.getTicksToFillActionBar();

	_.each(this.units, function (unit) {
		unit.tickActionBar(ticksLeft);
		unit.render();

		if (unit.actionBarFill > 1000) {
			console.warn("Filled > 1000", unit.name);
		}
	});

	nextUnit.beginTurn();
	nextUnit.render();
};

UnitList.prototype.seedActionBars = function() {
	var max = parseInt($('input.seedMax').val()) || Game.Settings.actionBarFull;

	_.each(this.units, function (unit) {
		unit.actionBarFill = Math.round(Math.random() * max);
		unit.$el.removeClass('active-turn');
	});

	this.units.sort(UnitList.sorter);

	_.each(this.units, function (unit) {
		unit.render();
		this.$el.append(unit.$el);
	}, this);

	this._firstScan = true;
};

UnitList.sorter = function (unit1, unit2) {
	ticks1 = unit1.getTicksToFillActionBar();
	ticks2 = unit2.getTicksToFillActionBar();

	if (ticks1 < ticks2) return -1;
	if (ticks1 > ticks2) return 1;
	return 0;
};

UnitList.prototype.killUnit = function (unit) {
	var ndx = this.units.indexOf(unit);
	if (ndx < 0) {
		console.error("Unit not found", unit);
		return;
	}

	unit.kill();
	this.units.splice(ndx, 1);
};
